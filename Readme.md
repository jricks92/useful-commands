Useful commands:
================

## Forward SSH Port from remote host to localhost
```bash
ssh -L 12345:localhost:80 user@remotemachine
```

## SSH via SOCKS proxy
```bash
ssh -o ProxyCommand='nc -x myproxyserver.example.com:1080 %h %p' targetsshserver.example.com
```

## Raspberry Pi
Remote Desktop Client
```bash
sudo apt-get install reman remmina-plugin-rdp
```
OR
```bash
sudo apt-get install rdesktop
```

## Remote Desktop Server (includes RDP and VNC)
```bash
sudo apt-get install xrdp
```

## Firefox
```bash
sudo apt-get install iceweasel
```
(With flash)
```bash
sudo apt-get install iceweasel browser-plugin-gnash
```

## Configure rPi to be a Time Machine server with AFP
- https://raymii.org/s/articles/Build_a_35_dollar_Time_Capsule_-_Raspberry_Pi_Time_Machine.html
- http://www.howtogeek.com/139433/how-to-turn-a-raspberry-pi-into-a-low-power-network-storage-device/
- https://www.raspberrypi.org/forums/viewtopic.php?f=36&t=26826
- https://daniel-lange.com/archives/102-Apple-Timemachine-backups-on-Debian-8-Jessie.html


## Remove GUI from rPi
http://raspberrypi.stackexchange.com/questions/34330/raspbian-without-a-gui-and-other-programs-i-dont-need

## Spin Down HDD Power on rPi
http://www.htpcguides.com/spin-down-and-manage-hard-drive-power-on-raspberry-pi/

## Detect bad HDD with SMART tools
https://www.raspberrypi.org/forums/viewtopic.php?f=36&t=47918

## Create Encrypted Sparsebundle for Time Machine over SMB
http://chester.me/archives/2013/04/a-step-by-step-guide-to-configure-encrypted-time-machine-backups-over-a-non-time-capsule-network-share.html/

## Removing Apple GateKeeper:
```bash
xattr -d com.apple.quarantine myfilename
```


## Limit Local Time Machine Backup Size
```bash
sudo defaults write /Library/Preferences/com.apple.TimeMachine MaxSize 768000
```
(Where 750GB x 1024 = 768000)

## Prevent app from automatically switching to discrete GPU on run
```bash
defaults write /[Path to Application]/Contents/Info.plist NSSupportsAutomaticGraphicsSwitching -bool YES
```

## SSH to iCloud Computer
```bash
ssh -2 -6 [username]@[computer name].38067620.members.btmm.icloud.com
```

## Prevent VMWare from telling that it’s a virtual machine
```bash
monitor_control.restrict_backdoor = "true"
```
## Allow virtualization applications on VMWare
```bash
vhv.enable = “true”
```

## DVD player patch for Internal made External USB on El Capitan:
1. Restart System and press directly after the tone Command + R-Tasten until the Apple-Logo appears
2. open Terminal from menu
3. execute "csrutil disable"
4. Restart OS X regulary
5. Open Terminal
    ```bash
    name=/System/Library/Frameworks/DVDPlayback.framework/Versions/A/DVDPlayback; sudo perl -i.saved -pe "s,\000Internal\000,\000External\000,sg" "$name";
    ```
6. if entered correctly, you'll be requested to enter your password
7. Restart System and press directly after the tone Command + R-Tasten until the Apple-Logo appears
8. open Terminal from menu
```bash
csrutil enable
```
9. Restart


## Convert UTF-16 to UTF-8 files:
```bash
iconv -f utf-16 -t utf-8 originalfile > newfile
```

## Add AirDrop on all interfaces:
```
defaults write com.apple.NetworkBrowser BrowseAllInterfaces 1
killall Finder
```


## Get SSL Cert Passphrase
```bash
sudo getsslpassphrase “*:443” RSA
```
Result: EFC3FF93-1CBF-490D-A816-EB9AB17FBD91

## Remove PEM Password
```bash
openssl rsa -in cert_with_pass.key -out cert.key
```

## Ignore Thunderbolt Firmware Update
```bash
softwareupdate --ignore ThunderboltFirmwareUpdate1.2
```

## Configuring Ubuntu RAID1+LVM:
http://blogging.dragon.org.uk/installing-ubuntu-14-04-on-raid-1-and-lvm/

## Turn on and off dnsmasq
```bash
sudo brew services start dnsmasq
sudo brew services stop dnsmasq
```

## Plex DNS Rebind on DD-WRT
Go to Services -> Additional DNSMasq options and input:
```bash
rebind-domain-ok=/plex.direct/
```

## Add and delete static route in OS X
```bash
sudo route -n add -net 10.67.0.0/16  192.168.120.254
```

## Backup script that checks for existence of backup drive before it starts the backup. Can be put in a cron job 
(Source: http://serverfault.com/questions/269996/optimal-backup-to-swappable-hard-drive-with-rsync-on-osx)
```bash
if [ -d /Volumes/Backup ]; then
    #sudo rsync -vaxE --delete --ignore-errors / /Volumes/Backup/
    sudo rsync -xrlptgoEv --progress --delete / /Volumes/Backup/
else
    # email, SMS, or DM a warning that the overnight backup didn't happen
fi
```

## Backup running VM on ESXi by creating snapshot with script
https://www.jamescoyle.net/how-to/313-backup-esxi-5-x-running-virtual-machine
https://www.veeam.com/blog/how-to-move-a-vmware-vm-from-one-host-to-another-without-vmware-vmotion.html

## Change resolution from terminal on OS X guest VMWare
http://www.virtuallyghetto.com/2015/10/heads-up-workaround-for-changing-mac-os-x-vm-display-resolution-in-vsphere-fusion.html

## Symbolic Link in Windows (admin prompt)
```bash
mklink /D “Target” “Source”
```

## Find IP address from NetBIOS name on OS X
```bash
smbutil lookup HOSTNAME
```

## Time machine on unsupported network volume
```bash
sudo defaults write com.apple.systempreferences TMShowUnsupportedNetworkVolumes 1
```

http://lifehacker.com/5685547/how-to-set-up-time-machine-to-back-up-to-a-networked-windows-or-linux-machine
## Limit time machine backups (Haven’t gotten to work on El Capitan)
https://www.youtube.com/watch?v=Nq7mSizqUSI&feature=youtu.be

## Add User to group on Linux
http://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/

## Rip Xbox 360 games for Xenia emulator
https://www.reddit.com/r/emulation/comments/4uxeo0/how_to_rip_your_xbox_360_and_xbla_games_for_use/

## Completely clear bash history
```bash
cat /dev/null > ~/.bash_history && history -c && exit
```

## Clear PowerShell History on Windows 10
```powershell
Remove-Item (Get-PSReadlineOption).HistorySavePath
```

## Remove Windows 10 App from PowerShell
```powershell
Get-AppxPackage *BingTranslator* | Remove-AppxPackage
Get-AppxPackage *Wunderlist* | Remove-AppxPackage
Get-AppxPackage *RichardWalters* | Remove-AppxPackage
```

## Restore Windows Store Apps
http://superuser.com/questions/949112/restore-microsoft-store-application-in-windows-10

## Refresh Windows Store Cache
1. Press the Win+R keys to open the Run dialog. 
2. Type WSReset.exe, and click/tap on OK.

## Set Powershell execution policy in powershell
```powershell
Enable-PSRemoting -Force
Set-ExecutionPolicy remotesigned (Say yes to all)
```

## Colors in terminal
```bash
export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\W\[\033[m\]\$ "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad
alias ls='ls -GFh'
```

## Git branch in  command prompt prompt.

edit ~/.bash_profile and add this:
```bash
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "
```

Or with colors:
```bash
export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\W\[\033[m\]\$(parse_git_branch)\$ "
```

## Make raw disk VMDK for VMWare Fusion
#### Check disk
```bash
/Applications/VMware\ Fusion.app/Contents/Library/vmware-rawdiskCreator print /dev/disk#
```
#### Create VMDK
```bash
/Applications/VMware\ Fusion.app/Contents/Library/vmware-rawdiskCreator create /dev/disk# <partNums> <virtDiskPath> ide
```
- `<partNums>` can be a comma-separated list of partitions to allow access to, such as "2" or "2,3". The numbers should match the output of the rawdiskCreator print command. Or to access the full disk, specify "fullDevice" instead of partition numbers.
- `<virtDiskPath>` should be a path inside the virtual machine bundle followed by a filename prefix, for example: ~/Documents/MyVM.vmwarevm/rawDiskFile

#### Add to VMX file 
```
ide0:0.present = "TRUE"
ide0:0.fileName = "rawDiskFile.vmdk"
ide0:0.deviceType = "rawDisk"
suspend.disabled = "TRUE"
```

## Switch between EFI and BIOS mode in VMWare VMX
```
firmware=“efi”
```
OR
```
firmware="bios"
```

## Useful GPG command line tricks
http://blog.ghostinthemachines.com/2015/03/01/how-to-use-gpg-command-line/

## Speedtest.net from CLI
https://www.howtoforge.com/tutorial/check-internet-speed-with-speedtest-cli-on-ubuntu/

## Let’s Encrypt SSL Certificates on OS X server
https://community.letsencrypt.org/t/complete-guide-to-install-ssl-certificate-on-your-os-x-server-hosted-website/15005

## Enable HiDPi resolutions in OS X
```bash
sudo defaults write /Library/Preferences/com.apple.windowserver.plist DisplayResolutionEnabled -bool true
```

## Enable 4K@60Hz on OS X
https://www.tonymacx86.com/threads/gtx960-hdmi-2-0-4k-tv-30hz-only-how-to-get-60hz.164495/page-9#post-1374737
1. Patch IOKit (https://github.com/Floris497/mac-pixel-clock-patch-V2)
2. Add custom resolution (3840x2160@60) via SwitchResX
3. Find the monitor definition file, in my case /System/Library/Displays/Contents/Resources/Overrides/DisplayVendorID-4c2d/DisplayYearManufacture-2016-DisplayWeekManufacture-1
4. Go to https://comsysto.github.io/Display-Override-PropertyList-File-Parser-and-Generator-with-HiDPI-Support-For-Scaled-Resolutions/, add the HiDPI modes you which to have into the web interface and copy&paste the "scale-resolutions" key and the array node below into your monitor definition file.
5. Reboot
6. While in SysPrefs->Display hold down the option key while clicking on "Scaled" to get a full list of possible resolutions instead of the Retina-Five-Button choice. Select your preferred HiDPI mode and select 60 Hz as refresh rate.
7. Enjoy.

## Enable RW capabilities of NTFS on OS X using Homebrew
https://coolestguidesontheplanet.com/how-to-write-to-ntfs-external-disk-drives-from-os-x-10-11-el-capitan/

## Harden OS X
http://docs.hardentheworld.org/OS/OSX_10.11_El_Capitan/

## Disable Bonjour advertisements
https://discussions.apple.com/message/28975763#message28975763

## Force Ubuntu to use IPv4 instead of IPv6
```bash
echo 'Acquire::ForceIPv4 "true";' | sudo tee /etc/apt/apt.conf.d/99force-ipv4
```

## Reset corrupt TCP/IP stack to prevent multiple gateways in Windows
Source: http://serverfault.com/questions/177353/my-windows-7-pc-receives-a-double-gateway-from-the-dhcp-server-why
```powershell
netsh winsock reset
netsh int ip reset
```

## Virtualizing FreeNAS precautions
http://www.freenas.org/blog/yes-you-can-virtualize-freenas/

## FreeNAS information for noobs
https://forums.freenas.org/index.php?threads/slideshow-explaining-vdev-zpool-zil-and-l2arc-for-noobs.7775/

## FileVault 2 on Hackintosh
http://www.insanelymac.com/forum/topic/317290-filevault-2/page-1

## GPU Passthrough VMX edits
```
pciHole.start = "1200"
pciHole.end = "2200"
```
OR
```
pciHole.start = "2048"
```

## GPU passthrough with NVIDIA in VMWare
https://linustechtips.com/main/topic/644362-vmware-esxi-600u2-nvidia-gpu-passthru/
```
hypervisior.cpuid.v0 = "FALSE"
pciHole.start = "2048"
```

## Force VMware to boot to bios using vmx file
```
bios.forceSetupOnce = "TRUE"
```

## Dummy Connectors for VGA and DVI
￼
![alt text](images/vga_dummy_electronic_schema.jpg "VGA Pinout with Resistors")

￼![alt text](images/vga_dummy_electronic_dvi.jpg "DVI Pinout with Resistors")


## Prevent macOS from automounting a drive
https://discussions.apple.com/docs/DOC-7942

1. Make sure the disk you want to prevent mounting at boot is mounted.
```bash
diskutil info /Volumes/<volume that shouldn't be mounted>
```
2. Edit (or create) an fstab file by typing the following and pressing enter:
```bash
sudo vifs
```
3. Enter the following line, substituting the UUID and filesystem type for your drive
```bash
UUID=FF9DBDC4-F77F-3F72-A6C2-26676F39B7CE none hfs rw,noauto
```
4. (Optional) Type the following and press enter to reset the auto mounter:
```bash
sudo automount -vc
```

## Remove old Kernels from Ubuntu
```
dpkg --list | grep linux-image | awk '{ print $2 }' | sort -V | sed -n '/'`uname -r`'/q;p' | xargs sudo apt-get -y purge
```


## Find public IP address in terminal
```bash
curl ipinfo.io/ip
```

## Disable GateKeeper on macOS Sierra
```bash
sudo spctl --master-disable
```
to re-enable:
```bash
sudo spctl --master-enable
```

## Multiple Interfaces and Gateways in Linux
https://www.thomas-krenn.com/en/wiki/Two_Default_Gateways_on_One_System

Add a new routing table to /etc/iproute2/rt_tables and put the preference number before it. Ex: 1 rt2
```
iface eth1 inet static
    address 10.10.0.10
    netmask 255.255.255.0
    post-up ip route add 10.10.0.0/24 dev eth1 src 10.10.0.10 table rt2
    post-up ip route add default via 10.10.0.1 dev eth1 table rt2
    post-up ip rule add from 10.10.0.10/32 table rt2
    post-up ip rule add to 10.10.0.10/32 table rt2
```


## Set default nameserver in Ubuntu
```bash
sudo nano /etc/resolvconf/resolv.conf.d/base
```
Add the following
```bash
nameserver 208.67.222.222
nameserver 208.67.220.220
```
Then run the command:
```bash
sudo resolvconf -u
```

## Find volume UUID on linux
```bash
sudo blkid
```

## Forward ESXi Embedded Host Client through SSH to be accessible via VMWare Fusion
```bash
sudo ssh root@10.10.1.2 -L localhost:8443:10.0.2.250:443 -N -L localhost:902:10.0.2.250:90
```

## Font Smoothing in Wine
```bash
winetricks settings fontsmooth=rgb
```

## Mount NFSv3 shares on OS X
```
nfs://nas.example.com/share_name
```

## Mount NFSv4 shares on OS X
```
nfs://vers=4,nas.example.com/share_name
```

## Prevent .DS_Store from being created on network drives
```bash
defaults write com.apple.desktopservices DSDontWriteNetworkStores true
```

## Prevent SSH host from being added to known_hosts file
Add this to SSH command:  `-o "UserKnownHostsFile /dev/null"`

## Remove ._ files from linux drive
```bash
find . -iname '._*' -exec rm -rf {} \;
```

## run command as www-data user
```bash
su - www-data -s /bin/bash -c 'wp plugin list'
```

## Create SOCKS proxy through SSH with no remote prompt (-D <port> creates the proxy)
```bash
ssh -D <local port> -f -C -q -N username@VPS.IP -p <ssh port>
```

## Restart macOS camera software
Sometimes the Camera will randomly stop working on my 2012 Retina MacBook Pro. This command fixes it.
```bash
sudo killall VDCAssistant
```

## Change Windows "PortableOS" flag
To turn on:
```powershell
reg add HKLM\SYSTEM\CurrentControlSet\Control /v PortableOperatingSystem /t REG_DWORD /d 1
```
To turn off:
```powershell
reg add HKLM\SYSTEM\CurrentControlSet\Control /v PortableOperatingSystem /t REG_DWORD /d 0
```

## Change entrypoint in docker on run/start bash directly
```bash
docker run -it --entrypoint=/bin/bash $IMAGE
```

## Enable/Disable Maintenance Mode with VMWare ESXCLI
```bash
esxcli system maintentanceMode set --enable true
esxcli system maintentanceMode set --enable false
```

## Remove all Docker containers and images
```bash
docker rm $(docker stop $(docker ps -a -q)) && docker rmi $(docker images -q)
```

## Disk speed test using dd
1. If using FreeNAS, create a dataset which has compression turned off.  This is important because compression 
will give you a 
false reading.
2. Open up a shell window
3. Type `dd if=/dev/zero of=/mnt/pool/dataset/test.dat bs=2048k count=10000`. This creates a 20GB file,
4. Note the results.
5. Type `dd of=/dev/null if=/mnt/pool/dataset/test.dat bs=2048k count=10000`
6. Note the results.
7. Lastly cleanup your mess and `rm /mnt/pool/dataset/test.dat` to delete the file you just created.
